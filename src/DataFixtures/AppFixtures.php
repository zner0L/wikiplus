<?php

namespace App\DataFixtures;

use App\Entity\Activity;
use App\Entity\ActivityType;
use App\Entity\Category;
use App\Entity\Event;
use App\Entity\EventType;
use App\Entity\GenderGroup;
use App\Entity\GroupPhase;
use App\Entity\ModerationState;
use App\Entity\Visibility;
use App\Entity\ZimmEntry;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $gender_female = new GenderGroup("Mädchen*");
        $gender_male = new GenderGroup("Jungs*");

        $feminism = new Category('Feminismus', '71187c');

        $demo_method = new Activity();
        $demo_method
            ->setType(ActivityType::METHOD())
            ->setTitle('Demo-Methode')
            ->setGroupPhase(GroupPhase::PERFORMING())
            ->setPeopleNumber(['min' => 5, 'max' => 10])
            ->setProcess('Schritt 1: alle knuddeln sich')
            ->setDuration(new \DateInterval('PT2H'))
            ->setMaterialList(array(
                "Papier für 10 Leute", "Stifte"
            ))
            ->addGenderGroup($gender_female)
            ->addGenderGroup(new GenderGroup("Genderfluid"))
            ->setGoal('Zusammenhalt in der Gruppe stärken')
            ->setComment('Nicht geeignet für Personen mit Traumaerfahrungen.')
            ->setDifficulty(2)
            ->setOwner($this->getReference(UserFixtures::DEMO_USER))
            ->setAge(['min' => 6, 'max' => 12])
            ->setCategory($feminism)
            ->setTags(['Femnism', '#metoo', 'blub'])
            ->setVisibility(Visibility::PUBLIC())
            ->setState(ModerationState::UNMODERATED());

        $demo_event = new Event();
        $demo_event
            ->setType(EventType::SEMINAR())
            ->setTopic('Feminismus erfinden')
            ->setText('In diesem Workshop sollen die Teilnehmenden spielerisch ihren eigenen Feminismus erfinden.')
            ->setCategory($feminism)
            ->setOwner($this->getReference(UserFixtures::DEMO_USER))
            ->setDifficulty(2)
            ->setComment('Nicht geeignet für Personen mit Traumaerfahrungen, wegen der ersten Methode.')
            ->setAge(['min' => 6, 'max' => 12])
            ->setVisibility(Visibility::PUBLIC())
            ->setState(ModerationState::UNMODERATED());


        $demo_event->addZimmEntry((new ZimmEntry())->setDuration(new \DateInterval('PT15M'))->setTitle('Ankommen'));
        $demo_event->addZimmEntry((new ZimmEntry())->setActivity($demo_method)->setMaterialList($demo_method->getMaterialList()));
        $demo_event->addZimmEntry((new ZimmEntry())->setDuration(new \DateInterval('PT5M'))->setTitle('Abschluss'));
        $demo_event->addZimmEntry((new ZimmEntry())->setDuration(new \DateInterval('PT30M'))->setTitle('Kochen')->setGoal('Es gibt was zu mampfen'));

        $manager->persist($gender_female);
        $manager->persist($gender_male);
        $manager->persist($feminism);
        $manager->persist($demo_method);
        $manager->persist($demo_event);

        $manager->flush();
    }

    public function getDependencies() {
        return array(
            UserFixtures::class,
        );
    }
}
