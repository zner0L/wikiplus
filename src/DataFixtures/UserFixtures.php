<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture {

    public const DEMO_USER = 'demo';

    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder) {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager) {
        $user = new User();

        $user->setName("Demon Strativ")
            ->setEmail("demon@strativ.test")
            ->setUsername('demo')
            ->setPassword($this->passwordEncoder->encodePassword($user, 'password'));

        $manager->persist($user);

        $manager->flush();

        $this->addReference(self::DEMO_USER, $user);
    }
}
