<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\KnowledgePoint;
use App\Entity\Section;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */
class AppController extends AbstractController {

    public function __construct() {
    }

    /**
     * @Route("/", name="app_index", methods={"GET"})
     */
    public function index() : Response {

        return $this->render('app/home.html.twig', [
            'disable_topic_nav' => true,
        ]);
    }

    /**
     * @Route("/theorie", name="topic_theory_index", methods={"GET"})
     */
    public function topicTheory() : Response {
        $knowledge_points = $this->getDoctrine()
            ->getRepository(KnowledgePoint::class)
            ->findBySection(Section::THEORY());

        return $this->render('app/home.html.twig', [
            'disable_topic_nav' => true,
        ]);
    }

    /**
     * @Route("/bildung", name="topic_education_index", methods={"GET"})
     */
    public function topicEducation() : Response {
        $knowledge_points = $this->getDoctrine()
            ->getRepository(KnowledgePoint::class)
            ->findBySectionGroupByCategory(Section::EDUCATION());

        $categories = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findAll();

        return $this->render('app/education.html.twig', [
            'knowledge_points' => $knowledge_points,
            'current_topic' => 'education',
            'categories' => $categories,
        ]);
    }

    /**
     * @Route("/organisation", name="topic_organisation_index", methods={"GET"})
     */
    public function topicOrganisation() : Response {
        $knowledge_points = $this->getDoctrine()
            ->getRepository(KnowledgePoint::class)
            ->findBySection(Section::ORGANISATION());

        return $this->render('app/home.html.twig', [
            'disable_topic_nav' => true,
        ]);
    }


}
