<?php

namespace App\Controller;

use App\Entity\Activity;
use App\Form\Type\ActivityType;
use Limenius\Liform\Liform;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/activity")
 */
class ActivityController extends AbstractController {
    private $liform;

    public function __construct(Liform $liform) {
        $this->liform = $liform;
    }

    /**
     * @Route("/", name="activity_index", methods={"GET"})
     */
    public function index() : Response {
        $activities = $this->getDoctrine()
            ->getRepository(Activity::class)
            ->findAll();

        return $this->render('activity/list.html.twig', [
            'activities' => $activities,
            'current_topic' => 'education',
        ]);
    }

    /**
     * @Route("/new", name="activity_new", methods={"GET","POST"})
     */
    public function new(Request $request) : Response {
        $activity = new Activity();
        $form = $this->createForm(ActivityType::class, $activity);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($activity);
            $entityManager->flush();

            return $this->redirectToRoute('activity_show', [
                'id' => $activity->getId(),
            ]);
        }

        return $this->render('activity/new.html.twig', [
            'activity' => $activity,
            'form' => $form->createView(),
            'current_topic' => 'education',
        ]);
    }

    /**
     * @Route("/{id}", name="activity_show", methods={"GET"})
     */
    public function show(Activity $activity) : Response {
        return $this->render('activity/show.html.twig', [
            'activity' => $activity,
            'current_topic' => 'education',
        ]);
    }

    /**
     * @Route("/{id}/edit", name="activity_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Activity $activity) : Response {
        $form = $this->createForm(ActivityType::class, $activity, ['csrf_protection' => false]);
        $serializer = $this->get('serializer');
        $initial_values = $serializer->normalize($form);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('activity_show', [
                'id' => $activity->getId(),
            ]);
        }

        return $this->render('activity/edit.html.twig', [
            'activity' => $activity,
            'form' => $form->createView(),
            'initial_values' => $initial_values,
            'current_topic' => 'education',
        ]);
    }

    /**
     * @Route("/{id}", name="activity_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Activity $activity) : Response {
        if($this->isCsrfTokenValid('delete' . $activity->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($activity);
            $entityManager->flush();
        }

        return $this->redirectToRoute('activity_index');
    }
}
