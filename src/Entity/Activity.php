<?php /** @noinspection PhpDocMissingReturnTagInspection */

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use MyCLabs\Enum\Enum;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Class Event
 * @package App\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="activities")
 */
class Activity extends KnowledgePoint {
    /**
     * @ORM\Column(type="string")
     */
    private $type;

    /** @ORM\Column(type="string") */
    private $title = '';

    /** @ORM\Column(type="smallint") */
    private $min_people_number;

    /** @ORM\Column(type="smallint") */
    private $max_people_number;

    /** @ORM\Column(type="dateinterval") */
    private $duration;

    /** @ORM\Column(type="text") */
    private $process = '';

    /** @ORM\Column(type="text") */
    private $goal = '';

    /** @ORM\Column(type="string", length=10) */
    private $group_phase;

    /** @ORM\ManyToMany(targetEntity="GenderGroup", cascade={"persist"})
     *  @ORM\JoinTable(name="activity_gendergroup",
     *     joinColumns={@ORM\JoinColumn(name="activity", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="gender_group", referencedColumnName="id")}
     *     ) */
    private $gender_groups;

    /** @ORM\Column(type="simple_array") */
    private $material_list = [];

    public function getTitle() : string {
        return $this->title;
    }

    public function setTitle(string $title) : self {
        $this->title = $title;
        return $this;
    }

    public function getPeopleNumber() : array {
        return ['min' => $this->min_people_number, 'max' => $this->max_people_number];
    }

    public function setPeopleNumber(array $people_number_range) : self {
        $this->min_people_number = $people_number_range['min'];
        $this->max_people_number = $people_number_range['max'];
        return $this;
    }

    public function getDuration() : ?\DateInterval {
        return $this->duration;
    }

    public function setDuration(\DateInterval $duration) : self {
        $this->duration = $duration;
        return $this;
    }

    public function getProcess() : string {
        return $this->process;
    }

    public function setProcess(string $process) : self {
        $this->process = $process;
        return $this;
    }

    public function getGoal() : string {
        return $this->goal;
    }

    public function setGoal(string $goal) : self {
        $this->goal = $goal;
        return $this;
    }

    public function getGroupPhase() : GroupPhase {
        return new GroupPhase($this->group_phase);
    }

    public function setGroupPhase(GroupPhase $group_phase) : self {
        $this->group_phase = $group_phase;
        return $this;
    }

    public function getGenderGroups() : Collection {
        return $this->gender_groups;
    }

    public function addGenderGroup(GenderGroup $gender_group) : self {
        $this->gender_groups->add($gender_group);
        return $this;
    }

    public function removeGenderGroup(GenderGroup $gender_group) : self {
        $this->gender_groups->removeElement($gender_group);
        return $this;
    }

    public function getType() : ActivityType {
        return new ActivityType($this->type);
    }

    public function setType(ActivityType $type) : self {
        $this->type = $type->getValue();
        return $this;
    }

    /**
     * @throws \Exception
     */
    public function setSection(Section $section) : KnowledgePoint {
        throw new \Exception('Section can not be changed for Activity instances.');
    }

    public function __construct() {
        parent::__construct();
        $this->group_phase = GroupPhase::FORMING();
        $this->gender_groups = new ArrayCollection();
        $this->type = ActivityType::METHOD();
    }

    public function getMaterialList() : array {
        return $this->material_list;
    }

    public function setMaterialList(array $material_list) : self {
        $this->material_list = $material_list;

        return $this;
    }
}

/**
 * Class GroupPhase
 * @package App\Entity
 * @method static GroupPhase FORMING
 * @method static GroupPhase STORMING
 * @method static GroupPhase NORMING
 * @method static GroupPhase PERFORMING
 * @method static GroupPhase REFORMING
 */
class GroupPhase extends Enum {
    private const FORMING = "Forming";
    private const STORMING = "Storming";
    private const NORMING = "Norming";
    private const PERFORMING = "Performing";
    private const REFORMING = "Reforming";
}

/**
 * Class ActivityType
 * @package App\Entity
 * @method static ActivityType METHOD
 * @method static ActivityType GAME
 */
class ActivityType extends Enum {
    private const GAME = "Spiel";
    private const METHOD = "Methode";
}
