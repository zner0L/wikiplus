<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/** @ORM\Entity
 * @ORM\Table(name="categories")
 */
class Category {
    /** @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue */
    private $id;

    /** @ORM\Column(type="string", unique=true) */
    private $name;

    /** @ORM\Column(type="string", length=8) */
    private $color;

    public function getColor() : string {
        return $this->color;
    }

    public function setColor(string $color) : self {
        $this->color = $color;
        return $this;
    }

    public function getName() : string {
        return $this->name;
    }

    public function setName(string $name) : self {
        $this->name = $name;
        return $this;
    }

    public function getId() : int {
        return $this->id;
    }

    public function __construct(string $name, string $color) {
        $this->name = $name;
        $this->color = $color;
    }

    public function __toString() : string {
        return $this->name;
    }
}
