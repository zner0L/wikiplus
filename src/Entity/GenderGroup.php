<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class GenderGroup
 * @package App\Entity
 *
 * @ORM\Entity
 */
class GenderGroup {
    /** @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue */
    private $id;

    /** @ORM\Column(type="string", length=30) */
    private $name;

    public function getName() : string {
        return $this->name;
    }

    public function setName(string $name) : self {
        $this->name = $name;
        return $this;
    }

    public function __toString() : string {
        return $this->getName();
    }

    public function getId() : int {
        return $this->id;
    }

    public function __construct(string $name) {
        $this->name = $name;
    }

}
