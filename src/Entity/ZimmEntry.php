<?php

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use MyCLabs\Enum\Enum;

/**
 * Class ZimmEntry
 * @package App\Entity
 *
 * A connection class to connect Activities with Events. Used to build ZIMMs.
 * I can either have one Activity, Method or Game or none.
 *
 * @ORM\Entity
 * @ORM\Table(name="zimm_entries")
 */
class ZimmEntry {
    /** @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue */
    private $id;

    /** @ORM\Column(type="string") */
    private $type;

    /** @ORM\ManyToOne(targetEntity="Activity") */
    private $activity = null;

    /** @ORM\Column(type="dateinterval") */
    private $duration;

    /** @ORM\Column(type="text") */
    private $description = '';

    /** @ORM\Column(type="string") */
    private $title = '';

    /** @ORM\Column(type="text") */
    private $goal = '';

    /** @ORM\Column(type="simple_array", nullable=true) */
    private $material_list;

    /**
     * @ORM\ManyToOne(targetEntity="Event", inversedBy="zimm_entries")
     * @ORM\JoinColumn(name="event_id", referencedColumnName="id")
     */
    private $parent_event;

    /**
     * @ORM\Column(type="json_array")
     */
    private $event_day = [1 => ''];

    public function getDescription() : string {
        return $this->description;
    }

    public function setDescription(string $description) : self {
        $this->description = $description;
        return $this;
    }

    public function getDuration() : ?\DateInterval {
        return $this->duration;
    }

    public function getActivity() : ?Activity {
        return $this->activity;
    }

    public function getTitle() : string {
        return $this->title;
    }

    public function setTitle(string $title) : self {
        $this->title = $title;
        return $this;
    }

    public function getMaterialList() : ?array {
        return $this->material_list;
    }

    public function setMaterialList(?array $material_list) : self {
        $this->material_list = $material_list;
        return $this;
    }

    public function getGoal() : string {
        return $this->goal;
    }

    public function setGoal(string $goal) : self {
        $this->goal = $goal;
        return $this;
    }

    public function setActivityOld(Activity $activity, array $event_day = [1 => '']) : void {
        $this->type = ZimmEntryType::ACTIVITY();
        $this->duration = $activity->getDuration();
        $this->title = $activity->getTitle();
        $this->description = '';
        $this->activity = $activity;
        $this->event_day = $event_day;
        $this->material_list = $activity->getMaterialList();
        $this->goal = $activity->getGoal();
    }

    public function setActivity(?Activity $activity) : self {
        $this->activity = $activity;
        if($activity) {
            $this->duration = $activity->getDuration();
            if($this->title == '') $this->title = $activity->getTitle();
        }

        return $this;
    }

    public function setParentEvent(Event $parent_event) {
        $this->parent_event = $parent_event;

        return $this;
    }

    public function getParentEvent() : Event {
        return $this->parent_event;
    }

    public function getType() : ZimmEntryType {
        return new ZimmEntryType($this->type);
    }

    public function setType(ZimmEntryType $type) : self {
        $this->type = $type->getValue();
        if($type === ZimmEntryType::CUSTOM()) $this->activity = null;

        return $this;
    }

    public function getEventDay() : array {
        return $this->event_day;
    }

    public function setEventDay(array $event_day) : self {
        $this->event_day = $event_day;

        return $this;
    }

    public function getId() {
        return $this->id;
    }

    public function setCustom(\DateInterval $duration, string $title, string $desc = '', array $material_list = [], string $goal = '', array $event_day = [1 => '']) : void {
        $this->type = ZimmEntryType::CUSTOM();
        $this->activity = null;
        $this->duration = $duration;
        $this->title = $title;
        $this->description = $desc;
        $this->event_day = $event_day;
        $this->material_list = $material_list;
        $this->goal = $goal;
    }

    public function __construct() {
        $this->type = ZimmEntryType::CUSTOM();
        $this->duration = new \DateInterval('PT0S');
    }

    public function remove() : void {
        $this->parent_event->removeZimmEntry($this);
    }

    public function setDuration(\DateInterval $duration) : self {
        $diff = Event::subDateIntervals($this->duration, $duration);
        if($this->parent_event) $this->parent_event->updateDuration($diff);
        $this->duration = $duration;

        return $this;
    }
}

/**
 * Class ZimmEntryType
 * @package App\Entity
 * @method static ZimmEntryType ACTIVITY
 * @method static ZimmEntryType CUSTOM
 */
class ZimmEntryType extends Enum {
    private const ACTIVITY = 'Activity';
    private const CUSTOM = 'Custom';
}
