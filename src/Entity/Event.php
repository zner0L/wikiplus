<?php
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use MyCLabs\Enum\Enum;

/**
 * Class Event
 * @package App\Entity
 *
 * @ORM\Entity
 */
class Event extends KnowledgePoint {
    /** @ORM\Column(type="string") */
    private $type;

    /** @ORM\Column(type="text") */
    private $topic = '';

    /** @ORM\Column(type="text") */
    private $text = '';

    /** @ORM\Column(type="dateinterval") */
    private $duration;

    /**
     * @ORM\OneToMany(
     *     targetEntity="ZimmEntry",
     *     mappedBy="parent_event",
     *     cascade={"persist", "remove"},
     *     orphanRemoval=true,
     * )
     * TODO: Add the complicated getters and setters
     */
    private $zimm_entries;

    /** @ORM\Column(type="string", nullable=true) */
    private $title;

    public function getTitle() : ?string {
        return $this->title;
    }

    public function setTitle(?string $title) : self {
        $this->title = $title;
        return $this;
    }

    public function getText() : string {
        return $this->text;
    }

    public function setText(string $text) : self {
        $this->text = $text;
        return $this;
    }

    public function getTopic() : string {
        return $this->topic;
    }

    public function setTopic(string $topic) : self {
        $this->topic = $topic;
        return $this;
    }

    public function updateDuration(\DateInterval $diff) : self {
        $this->duration = self::addDateIntervals($this->duration, $diff);

        return $this;
    }

    public function getDuration() : \DateInterval {
        return $this->duration;
    }

    public function getType() : EventType {
        return new EventType($this->type);
    }

    public function setType(EventType $type) : self {
        $this->type = $type->getValue();
        return $this;
    }

    public function getZimmEntries() : Collection {
        return $this->zimm_entries;
    }

    /**
     * @throws \Exception
     */
    public function setSection(Section $section) : KnowledgePoint {
        throw new \Exception('Section can not be changed for Event instances.');
    }

    public function __construct() {
        parent::__construct();
        $this->zimm_entries = new ArrayCollection();
        $this->duration = new \DateInterval('PT0S');
        $this->type = EventType::WORKSHOP();
    }

    public function addZimmEntry(ZimmEntry $entry) {
        $this->zimm_entries->add($entry);
        $entry->setParentEvent($this);
        $this->duration = key($entry->getEventDay()) < 1 ? self::addDateIntervals($this->duration, $entry->getDuration()) : new \DateInterval('P' . key($entry->getEventDay()) . 'D');
    }

    public function removeZimmEntry(ZimmEntry $entry) {
        $this->zimm_entries->removeElement($entry); // TODO: Check if it is removed and existed before that
        $this->duration = self::subDateIntervals($this->duration, $entry->getDuration());
    }

    public function removeZimmEntryByKey($key) {
        $this->duration = self::subDateIntervals($this->duration, $this->zimm_entries[$key]->getDuration());
        $this->zimm_entries->remove($key);
    }

    public static function addDateIntervals(\DateInterval $a, \DateInterval $b) {
        $reference = new \DateTime();
        $endTime = clone $reference;

        $endTime->add($a);
        $endTime->add($b);

        return $reference->diff($endTime);
    }

    public static function subDateIntervals(\DateInterval $a, \DateInterval $b) {
        $reference = new \DateTime();
        $endTime = clone $reference;

        $endTime->add($a);
        $endTime->sub($b);

        return $reference->diff($endTime);
    }
}

/**
 * Class EventType
 * @package App\Entity
 * @method static EventType SEMINAR
 * @method static EventType WORKSHOP
 * @method static EventType GROUP_MEETING
 */
class EventType extends Enum {
    private const SEMINAR = "Seminar";
    private const WORKSHOP = "Workshop";
    private const GROUP_MEETING = "Gruppenstunde";
}
