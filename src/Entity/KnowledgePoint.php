<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use MyCLabs\Enum\Enum;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class KnowledgePoint
 * @package App\Entity
 *
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\KnowledgePointRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"knowledge_point" = "KnowledgePoint", "activity" = "Activity", "event" = "Event"})
 * @ORM\HasLifecycleCallbacks
 */
class KnowledgePoint {
    /** @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue  */
    private $id;

    /** @ORM\Column(type="simple_array", nullable=true) */
    private $tags = [];

    /** @ORM\Column(type="integer") */
    private $min_age;

    /** @ORM\Column(type="integer") */
    private $max_age;

    /** @ORM\Column(type="integer") */
    private $difficulty = 0;

    /** @ORM\Column(type="text") */
    private $comment = '';

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @ORM\Column(type="integer")
     */
    private $visibility;

    /**
     * @ORM\Column(type="integer")
     */
    private $state;

    /** @ORM\Column(type="datetime") */
    private $createdAt;

    /** @ORM\Column(type="datetime") */
    private $updatedAt;

    /** @ORM\Column(type="string", length=12) */
    private $section;

    public function getAge() : array {
        return ['min' => $this->min_age, 'max' => $this->max_age];
    }

    public function setAge(array $age_range) : self {
        $this->min_age = $age_range['min'];
        $this->max_age = $age_range['max'];
        return $this;
    }

    public function getOwner() : ?User {
        return $this->owner;
    }

    public function setOwner(User $owner) : self {
        $this->owner = $owner;
        return $this;
    }

    public function getVisibility() : Visibility {
        return new Visibility($this->visibility);
    }

    public function setVisibility(Visibility $visibility) : self {
        $this->visibility = $visibility->getValue();
        return $this;
    }

    public function getState() : ModerationState {
        return new ModerationState($this->state);
    }

    public function setState(ModerationState $state) : self {
        $this->state = $state->getValue();
        return $this;
    }

    public function getTags() : array {
        return $this->tags;
    }

    public function setTags(array $tags) : self {
        $this->tags = $tags;
        return $this;
    }

    public function getDifficulty() : int {
        return $this->difficulty;
    }

    public function setDifficulty(int $difficulty) : self {
        $this->difficulty = $difficulty;
        return $this;
    }

    public function getComment() : string {
        return $this->comment;
    }

    public function setComment(string $comment) : self {
        $this->comment = $comment;
        return $this;
    }

    public function getCategory() : ?Category {
        return $this->category;
    }

    public function setCategory(Category $category) : self {
        $this->category = $category;
        return $this;
    }

    public function getId() : int {
        return $this->id;
    }

    public function getUpdatedAt() : \DateTime {
        return $this->updatedAt;
    }

    public function getCreatedAt() : \DateTime {
        return $this->createdAt;
    }

    public function setSection(Section $section) : self {
        $this->section = $section->getValue();
        return $this;
    }

    public function getSection() : Section {
        return new Section($this->section);
    }

    public function __construct() {
        // Set Defaults
        $this->visibility = Visibility::PRIVATE();
        $this->state = ModerationState::UNMODERATED();
        $this->section = Section::EDUCATION();
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist() : void {
        $this->createdAt = new \DateTime();
        $this->updatedAt = $this->createdAt;
    }

    /**
     * @ORM\PreUpdate
     */
    public function onPreUpdate() : void {
        $this->updatedAt = new \DateTime();
    }

    public function getClass() {
        return get_class($this);
    }
}

/**
 * Class Visibility
 * @package App\Entity
 * @method static Visibility PUBLIC
 * @method static Visibility PRIVATE
 * @method static Visibility AUTHENTICATED
 */
class Visibility extends Enum {
    private const PUBLIC = 0;
    private const PRIVATE = 1;
    private const AUTHENTICATED = 2;
}

/**
 * Class ModerationState
 * @package App\Entity
 * @method static ModerationState UNMODERATED
 * @method static ModerationState MODERATION_PENDING
 * @method static ModerationState PUBLISHED
 */
class ModerationState extends Enum {
    private const UNMODERATED = 0;
    private const MODERATION_PENDING = 1;
    private const PUBLISHED = 2;
}

/**
 * Class Section
 * @package App\Entity
 * @method static Section THEORY
 * @method static Section EDUCATION
 * @method static Section ORGANISATION
 */
class Section extends Enum {
    private const THEORY = 'theory';
    private const EDUCATION = 'education';
    private const ORGANISATION = 'organisation';
}
