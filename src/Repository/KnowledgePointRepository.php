<?php

namespace App\Repository;

use App\Entity\KnowledgePoint;
use App\Entity\Section;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KnowledgePointRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, KnowledgePoint::class);
    }

    public function findBySection(Section $section) {
        return $this->createQueryBuilder('k')
            ->andWhere('k.section = :sect')
            ->setParameter('sect', $section->getValue())
            ->orderBy('k.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function findBySectionGroupByCategory(Section $section) {
        return $this->createQueryBuilder('k')
            ->andWhere('k.section = :sect')
            ->setParameter('sect', $section->getValue())
            ->orderBy('k.category, k.createdAt')
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
