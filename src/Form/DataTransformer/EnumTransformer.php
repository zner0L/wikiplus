<?php

namespace App\Form\DataTransformer;

use Psr\Log\LoggerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Exception;
use MyCLabs\Enum\Enum;


class EnumTransformer implements DataTransformerInterface {
    private $enum_class = '';
    private $logger;

    public function __construct(string $enum_class) {
        $this->enum_class = $enum_class;
    }


    public function transform($data) {
        if($data === null){
            return null;
        } elseif($data instanceof $this->enum_class) {
            return $data->getValue();
        } else {
            return $data;
        }

    }

    public function reverseTransform($data) {
        return new $this->enum_class($data);
    }
}
