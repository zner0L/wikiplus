<?php

namespace App\Form\Type;

use Psr\Log\LoggerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Exception;


class EventDayType extends AbstractType implements DataMapperInterface {
    private $logger;

    public function __construct(LoggerInterface $logger) {
        $this->logger = $logger;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('day_number', IntegerType::class)
            ->add('day_label', TextType::class)
            ->setDataMapper($this)
        ;
    }

    public function mapDataToForms($data, $forms) {
        if($data == null) {
            return;
        } else if(gettype($data) === 'array') {
            $forms = iterator_to_array($forms);
            $forms['day_number']->setData(key($data));
            $forms['day_label']->setData(current($data));
        } else {
            throw new Exception\UnexpectedTypeException($data, 'array(int => string');
        }
    }

    public function mapFormsToData($forms, &$data) {
        $forms = iterator_to_array($forms);
        $data = [(int)$forms['day_number']->getData() => $forms['day_label']->getData()];
    }

}
