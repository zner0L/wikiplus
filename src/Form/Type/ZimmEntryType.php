<?php

namespace App\Form\Type;

use App\Entity\Activity;
use App\Entity\ZimmEntry;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateIntervalType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ZimmEntryType extends AbstractType {
    private $logger;

    public function __construct(LoggerInterface $logger) {
        $this->logger = $logger;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefault('data_class', ZimmEntry::class);
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('activity', EntityType::class, [
                'class' => Activity::class,
                'required' => false,
                'placeholder' => 'zimm_entry.activity.name',
                'choice_label' => function ($activity) {
                    return $activity->getTitle();
                },
            ])
            ->add('title', TextType::class, [
                'label' => 'zimm_entry.title',
                'empty_data' => '',
                'attr' => [
                    'placeholder' => 'zimm_entry.title'
                ]
            ])
            ->add('duration', DateIntervalType::class, [
                'widget' => 'text', // render a text field for each part
                'with_years' => false,
                'with_months' => false,
                'with_days' => false,
                'with_hours' => true,
                'with_minutes' => true,
                'empty_data' => [0,0],
                'label_format' => 'zimm_entry.duration.%name%',
            ])
            ->add('goal', TextType::class, [
                'label' => 'zimm_entry.goal.label',
                'empty_data' => '',
                'attr' => [
                    'placeholder' => 'zimm_entry.goal.placeholder'
                ]
            ])
            ->add('description', TextareaType::class, [
                'label' => 'zimm_entry.description.label.Custom',
                'empty_data' => '',
                'attr' => [
                    'placeholder' => 'zimm_entry.description.placeholder'
                ],
            ])
            ->add('material_list', SimpleArrayType::class, [
                'label' => 'zimm_entry.material.label',
                'required' => false,
                'empty_data' => '',
                'attr' => [
                    'placeholder' => 'zimm_entry.material.placeholder'
                ],
            ])
            ->add('event_day', EventDayType::class, [
                'label' => 'zimm_entry.event_day',
                'empty_data' => [1 => ''],
            ])
        ;
    }

}
