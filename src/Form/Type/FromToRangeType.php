<?php

namespace App\Form\Type;

use Psr\Log\LoggerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Exception;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FromToRangeType extends AbstractType implements DataMapperInterface {
    private $logger;

    public function __construct(LoggerInterface $logger) {
        $this->logger = $logger;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'empty_data' => ['min' => 0, 'max' => 10],
            'min' => 0,
            'max' => 10,
            'marks' => [0 => '0', 10 => '10'],
        ]);
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('min', IntegerType::class)
            ->add('max', IntegerType::class)
            ->setDataMapper($this)
        ;
    }

    public function buildView(FormView $view, FormInterface $form, array $options) {
        $form = iterator_to_array($form);

        $view->vars['attr']['data-id'] =  $view->vars['id'];
        $view->vars['attr']['data-min'] =  $options['min'];
        $view->vars['attr']['data-max'] =  $options['max'];
        $view->vars['attr']['data-name-min'] = $view->vars['full_name'] . '[min]';
        $view->vars['attr']['data-name-max'] =  $view->vars['full_name'] . '[max]';
        $view->vars['attr']['data-values'] = json_encode([$form['min']->getData() ?? $options['empty_data']['min'], $form['max']->getData()  ?? $options['empty_data']['max']]);
        $view->vars['attr']['data-marks'] = json_encode($options['marks']);
        $view->vars['attr']['class'] = (isset($view->vars['attr']['class']) ? $view->vars['attr']['class'] . ' ' : '') . 'react-range-slider';
    }

    /**
     * Maps properties of some data to a list of forms.
     *
     * @param mixed $data Structured data
     * @param FormInterface[]|\Traversable $forms A list of {@link FormInterface} instances
     *
     * @throws Exception\UnexpectedTypeException if the type of the data parameter is not supported
     */
    public function mapDataToForms($data, $forms) {
        if($data == null) {
            return;
        } else if(gettype($data) === 'array' && array_key_exists('min', $data) && array_key_exists('max', $data)) {
            $forms = iterator_to_array($forms);
            $forms['min']->setData($data['min']);
            $forms['max']->setData($data['max']);
        } else {
            throw new Exception\UnexpectedTypeException($data, 'array(\'min\' => int, \'max\' => int)');
        }
    }

    /**
     * Maps the data of a list of forms into the properties of some data.
     *
     * @param FormInterface[]|\Traversable $forms A list of {@link FormInterface} instances
     * @param mixed $data Structured data
     *
     * @throws Exception\UnexpectedTypeException if the type of the data parameter is not supported
     */
    public function mapFormsToData($forms, &$data) {
        $forms = iterator_to_array($forms);
        $data['min'] = $forms['min']->getData();
        $data['max'] = $forms['max']->getData();
    }
}
