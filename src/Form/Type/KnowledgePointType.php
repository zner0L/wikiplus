<?php

namespace App\Form\Type;

use App\Entity\Category;
use App\Entity\ModerationState;
use App\Entity\User;
use App\Entity\Visibility;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class KnowledgePointType extends AbstractType {
    private $t;
    private $logger;

    public function __construct(TranslatorInterface $t, LoggerInterface $logger) {
        $this->t = $t;
        $this->logger = $logger;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $t = $this->t;

        $builder
            ->add('tags', SimpleArrayType::class, [
                'label' => 'knowledge_point.tags',
                'required' => false,
            ])
            ->add('age', FromToRangeType::class, [
                'min' => 0,
                'max' => 28,
                'label_format' => 'knowledge_point.age.%name%',
                'marks' => [
                    0 => '0',
                    6 => $t->trans('knowledge_point.age.fring'),
                    16 => $t->trans('knowledge_point.age.sjring'),
                    28 => '27+',
                ],
                'required' => false,
            ])
            ->add('difficulty', RangeType::class, [
                'attr' => [
                    'min' => 1,
                    'max' => 3,
                    'data-marks' => json_encode([
                        1 => $t->trans('knowledge_point.difficulty.1'),
                        2 => $t->trans('knowledge_point.difficulty.2'),
                        3 => $t->trans('knowledge_point.difficulty.3'),
                    ]),
                ],
                'label' => 'knowledge_point.difficulty.name',
                'required' => false,
            ])
            ->add('comment', TextareaType::class, [
                'label' => 'knowledge_point.comment',
                'required' => false,
            ])
            ->add('visibility', EnumType::class, [
                'enum_class' => Visibility::class,
                'label' => 'knowledge_point.visibility',
                'required' => true,
            ])
            ->add('state', EnumType::class, [
                'enum_class' => ModerationState::class,
                'label' => 'knowledge_point.state',
                'required' => true,
            ])
            ->add('owner', EntityType::class, [
                'class' => User::class,
                'label' => 'knowledge_point.owner',
                'required' => true,
            ])
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'label' => 'knowledge_point.category',
                'required' => true,
            ])
        ;
    }
}
