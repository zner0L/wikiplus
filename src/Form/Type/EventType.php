<?php

namespace App\Form\Type;

use App\Entity\ZimmEntry;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateIntervalType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class EventType extends AbstractType {
    private $t;
    private $logger;

    public function __construct(TranslatorInterface $t, LoggerInterface $logger) {
        $this->t = $t;
        $this->logger = $logger;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $t = $this->t;

        $builder
            ->add('type', EnumType::class, [
                'enum_class' => \App\Entity\EventType::class,
                'label' => 'event.meta.type',
                'required' => true,
            ])
            ->add('topic', TextType::class, [
                'attr' => [
                    'placeholder' => 'event.meta.topic'
                ],
                'label' => false,
                'required' => false,
            ])
            ->add('text', QuillType::class, [
                'placeholder' => $t->trans('event.form.text_placeholder'),
                'label' => 'event.content.text',
                'required' => false,
            ])
            ->add('zimm_entries', CollectionType::class, [
                'entry_type' => ZimmEntryType::class,
                'by_reference' => false,
                'prototype' => true,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype_data' => new ZimmEntry(),
                'label' => 'zimm_entry.plural',
                'required' => false,
            ])
        ;
    }

    public function getParent() {
        return KnowledgePointType::class;
    }
}
