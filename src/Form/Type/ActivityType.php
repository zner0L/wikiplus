<?php

namespace App\Form\Type;

use App\Entity\Activity;
use App\Entity\GenderGroup;
use App\Entity\GroupPhase;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateIntervalType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class ActivityType extends AbstractType {
    private $t;
    private $logger;

    public function __construct(TranslatorInterface $t, LoggerInterface $logger) {
        $this->t = $t;
        $this->logger = $logger;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $t = $this->t;

        $builder
            ->add('type', EnumType::class, [
                'enum_class' => \App\Entity\ActivityType::class,
                'label' => 'activity.meta.type',
                'required' => true,
            ])
            ->add('title', TextType::class, [
                'attr' => [
                    'placeholder' => 'activity.meta.title',
                ],
                'label' => 'activity.meta.title',
                'required' => true,
            ])
            ->add('people_number', FromToRangeType::class, [
                'min' => 0,
                'max' => 30,
                'label_format' => 'activity.meta.people_number.%name%',
                'marks' => [
                    0 => '0',
                    5 => '5',
                    10 => '10',
                    20 => '20',
                    30 => '30+',
                ],
                'required' => false,
            ])
            ->add('duration', DateIntervalType::class, [
                'widget' => 'integer', // render a text field for each part
                'with_years' => false,
                'with_months' => false,
                'with_days' => false,
                'with_hours' => true,
                'with_minutes' => true,
                'label_format' => 'activity.meta.duration.%name%',
                'required' => true,
            ])
            ->add('process', QuillType::class, [
                'placeholder' => $t->trans('activity.form.content.process_placeholder'),
                'label' => 'activity.content.process',
                'required' => false,
            ])
            ->add('goal', TextType::class, [
                'label' => 'activity.content.goal',
                'required' => false,
            ])
            ->add('group_phase', EnumType::class, [
                'enum_class' => GroupPhase::class,
                'label' => 'activity.meta.group_phase',
                'required' => false,
            ])
            ->add('material_list', SimpleArrayType::class, [
                'label' => 'activity.content.material',
                'required' => false,
            ]) // TODO: Make tag-like entry
            ->add('gender_groups', EntityType::class, [
                'class' => GenderGroup::class,
                'multiple' => true,
                'label' => 'activity.meta.gender_groups',
                'required' => false,
            ])
        ;
    }

    public function getParent() {
        return KnowledgePointType::class;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Activity::class,
        ]);
    }
}
