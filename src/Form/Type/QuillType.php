<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuillType extends AbstractType {
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefault('purify_html', true);
        $resolver->setDefault('toolbar', [
            ['bold', 'italic', 'underline', 'strike'],
            [['header' => [1, 2, false]]],
            [['list' => 'ordered'], [ 'list' => 'bullet' ], ['indent' => '-1'], ['indent' => '+1']],
            ['link', 'image'],
            ['clean']
        ]);
        $resolver->setDefault('theme', 'snow');
        $resolver->setDefault('placeholder', '');
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {

    }

    public function buildView(FormView $view, FormInterface $form, array $options) {
        $view->vars['quill_config'] = [
            'modules' => [
                'toolbar' => $options['toolbar'],
            ],
            'theme' => $options['theme'],
            'placeholder' => $options['placeholder']
        ];
    }

    public function getParent() {
        return TextareaType::class;
    }
}
