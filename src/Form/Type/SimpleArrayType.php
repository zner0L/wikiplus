<?php

namespace App\Form\Type;

use Psr\Log\LoggerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class SimpleArrayType extends AbstractType implements DataTransformerInterface {
    private $logger;

    public function __construct(LoggerInterface $logger) {
        $this->logger = $logger;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->addModelTransformer($this);
    }

    public function getParent() {
        return TextType::class;
    }

    public function transform($data) {
        if($data == null) {
            return null;
        } else if(gettype($data) === 'array') {
            return join(', ', $data);
        } else {
            throw new Exception\UnexpectedTypeException($data, 'array');
        }
    }

    public function reverseTransform($data) {
        return explode(', ', $data);
    }
}
