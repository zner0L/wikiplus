<?php

namespace App\Form\Type;

use App\Form\DataMapper\EnumMapper;
use App\Form\DataTransformer\EnumTransformer;
use MyCLabs\Enum\Enum;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EnumType extends AbstractType {
    private $logger;

    public function __construct(LoggerInterface $logger) {
        $this->logger = $logger;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefined('enum_class');
        $resolver->setDefault('choices', function(Options $options) {
            return call_user_func($options['enum_class'] . '::toArray');
        });
        $resolver->setDefault('choice_translation_domain', 'enums');
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->addModelTransformer(new EnumTransformer($options['enum_class']));
    }

    public function getParent() {
        return ChoiceType::class;
    }
}
