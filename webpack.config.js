var Encore = require('@symfony/webpack-encore');

Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')
    // only needed for CDN's or sub-directory deploy
    //.setManifestKeyPrefix('build/')

    /*
     * ENTRY CONFIG
     *
     * Add 1 entry for each "page" of your app
     * (including one that's included on every page - e.g. "app")
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if you JavaScript imports CSS.
     */
    .addEntry('app', './assets/js/app.js')
    .addEntry('forms', './assets/js/forms.js')
    //.addEntry('page2', './assets/js/page2.js')

    // When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
    .splitEntryChunks()

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    // enables @babel/preset-env polyfills
    .configureBabel(() => {}, {
        useBuiltIns: 'usage',
        corejs: 3
    })

    // enables Sass/SCSS support
    .enableSassLoader()
    .enablePostCssLoader()

    // uncomment to get integrity="..." attributes on your script & link tags
    // requires WebpackEncoreBundle 1.4 or higher
    .enableIntegrityHashes()

    .enablePreactPreset({ preactCompat: true })
    //.enableReactPreset()

    // Add a custom loader to exclude quill assets (see here: https://github.com/symfony/webpack-encore/issues/393)
    .disableImagesLoader()
    .addRule({
        test: /\.(svg|png|jpg|jpeg|gif|ico)/,
        exclude: /node_modules\/quill\/assets\/icons\/(.*)\.svg$/,
        use: [{
            loader: 'file-loader',
            options: {
                filename: 'images/[name].[hash:8].[ext]',
                publicPath: '/build/'
            }
        }]
    })

    .addLoader({
        test: /node_modules\/quill\/assets\/icons\/(.*)\.svg$/,
        loader: 'html-loader',
        options: {
            minimize: true
        }
    })

;

let config = Encore.getWebpackConfig();
config.resolve.alias['react-dom'] = 'preact/compat';
config.resolve.alias['react'] = 'preact/compat';

module.exports = config;
