import Quill from 'quill/core';

import Toolbar from 'quill/modules/toolbar';
import Snow from 'quill/themes/snow';

import Bold from 'quill/formats/bold';
import Italic from 'quill/formats/italic';
import Underline from 'quill/formats/underline';
import Strike from 'quill/formats/strike';
import Link from 'quill/formats/link';
import {SizeClass} from 'quill/formats/size';
import Script from 'quill/formats/script';

import Header from 'quill/formats/header';
import List, {ListItem} from 'quill/formats/list';
import Image from 'quill/formats/image';
import {IndentClass} from 'quill/formats/indent';

Quill.register({
    'modules/toolbar': Toolbar,
    'themes/snow': Snow,
    'formats/bold': Bold,
    'formats/italic': Italic,
    'formats/header': Header,
    'formats/underline': Underline,
    'formats/strike': Strike,
    'formats/link': Link,
    'formats/size': SizeClass,
    'formats/script': Script,
    'formats/list': List,
    'formats/list-item': ListItem,
    'formats/image': Image,
    'formats/indent': IndentClass,
});


export default Quill;
