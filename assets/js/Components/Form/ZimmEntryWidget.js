import * as preact from 'preact';
import React from 'preact/compat';


export default class ZimmEntryWidget extends preact.Component{
    constructor(props) {
        super(props);

        this.state = {
            title: '' || props.title,
            duration: '' || props.duration,
            goal: '' || props.goal,
            description: '' || props.description,
            activity: '' || props.activity,
            materialList: '' || props.materialList,
            type: '',
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        switch (e.target.dataset.name) {
            case 'duration':
                let duration = e.target.value.match(/(0?[0-9]|1[0-9]|2[0-4]):[0-5][0-9]/);
                if(duration && duration[0]) {
                    this.setState({
                        duration: duration[0]
                    });
                } else {
                    this.setState({
                        duration: this.props.duration
                    });
                }
                break;
            case 'title':
            case 'goal':
            case 'description':
            case 'materialList':
            default:
                this.setState(prev => {
                    prev[e.target.dataset.name] = e.target.value;
                    return prev;
                });
        }
    }

    render() {
        return (
            <div className="card zimm_entry-card">
                <div className="card-header" data-toggle="collapse" data-target={'#' + this.props.id} aria-expanded="false"
                     aria-controls={this.props.id}>
                    <div className="zimm_entry-duration">
                        <input type="text" data-name="duration" className="zimm_entry-input zimm_entry-duration-input"
                               value={this.state.duration} onChange={this.handleChange} />
                    </div>
                    <h3 className="zimm_entry-title">
                        <input type="text" data-name="title" className="zimm_entry-input zimm_entry-duration-input" value={this.state.title} />
                    </h3>
                </div>
                <div id={this.props.id} className="collapse">
                    <ul className="list-group list-group-flush">
                        <li className="list-group-item">
                            <label htmlFor={this.props.id + '-goal-input'}>
                                <strong>{'zimm_entry.goal'}</strong>
                            </label>: <input id={this.props.id + '-goal-input'} type="text"
                                             className="zimm_entry-input zimm_entry-goal-input" value={ this.state.goal } />
                        </li>
                    </ul>
                    <ul className="list-group list-group-flush">
                        <li className="list-group-item list-group-item-action" data-toggle="collapse"
                            data-target={'#' + this.props.id + '-desc'} aria-expanded="false" aria-controls={this.props.id + '-desc'}>
                            <h4>{('zimm_entry.description.' + this.state.type)}</h4></li>
                    </ul>
                    <div className="collapse" id={ this.props.id + '-desc' }>
                        <div className="card-body">
                            <textarea data-name="description" className="card-text zimm_entry-input zimm_entry-description-input">
                                { this.state.description }
                             </textarea>
                        </div>
                    </div>
                    <ul className="list-group list-group-flush">
                        <li className="list-group-item list-group-item-action" data-toggle="collapse"
                            data-target={'#' + this.props.id + '-material'} aria-expanded="false" aria-controls={ this.props.id + '-material'}>
                            <h4>{'zimm_entry.material'}</h4></li>
                    </ul>
                    <div className="collapse" id={ this.props.id + '-material'}>
                        <div className="card-body">
                            <input data-name="materialList" type="text" className="zimm_entry-input zimm_entry-material-input" value={ this.state.materialList.join(', ') } />
                        </div>
                    </div>
                    <div className="card-footer clearfix">
                        <a className="btn btn-info" href="#">{'zimm_entry.activity.show.' + this.state.type}</a>
                        <button className="btn btn-danger float-right" onClick={this.props.onDelete}>{'zimm_entry.delete'}</button>
                    </div>
                </div>
            </div>
        );
    }
}
