import * as preact from "preact";
import React from 'preact/compat';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';

class QuillWidget extends preact.Component {
    constructor(props) {
        super(props);
        this.state = {
            html: ''
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(value, delta, source, editor) {
        this.setState({html: value});
    }

    render() {
        return (
            <div className={'quill-widget'}>
                <ReactQuill value={this.state.html} onChange={this.handleChange} {...this.props} />
            </div>);
    }
}
