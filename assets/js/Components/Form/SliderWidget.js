import * as preact from 'preact';
import React from 'preact/compat';
import Slider from 'rc-slider';
import Tooltip from 'rc-tooltip';
import classNames from "classnames";
import 'rc-slider/assets/index.css';

const Handle = Slider.Handle;

const handle = (props) => {
    const { value, dragging, index, ...restProps } = props;
    return (
        <Tooltip
            prefixCls="rc-slider-tooltip"
            overlay={value}
            visible={dragging}
            placement="top"
            key={'tooltip-' + index}
        >
            <Handle value={value} {...restProps} />
        </Tooltip>
    );
};

export default class SliderWidget extends preact.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: props.defaultValue,
        };

        let marks = {};
        marks[this.props.min] = this.props.min;
        marks[this.props.max] = this.props.max;

        this.props.marks = props.marks || marks;
        this.handleChange = this.handleChange.bind(this);
    }

    render() {

        return (
            <div id={this.props.id} className={classNames('rc-slider-container', this.props.className)}>
                <Slider
                    min={this.props.min}
                    max={this.props.max}
                    defaultValue={this.props.defaultValue}
                    step={this.props.step}
                    onChange={this.handleChange}
                    marks={this.props.marks}
                    handle={handle}
                />
                <input type="hidden" value={this.state.value} name={this.props.name} />
            </div>
        );
    }

    handleChange(value) {
        this.setState({value: value});
    }
}

