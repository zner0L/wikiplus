import * as preact from 'preact';
import React from 'preact/compat';
import Slider, {Range} from 'rc-slider';
import Tooltip from 'rc-tooltip';
import classNames from "classnames";
import 'rc-slider/assets/index.css';

const Handle = Slider.Handle;

const handle = (props) => {
    const { value, dragging, index, ...restProps } = props;
    return (
        <Tooltip
            prefixCls="rc-slider-tooltip"
            overlay={value}
            visible={dragging}
            placement="top"
            key={'tooltip-' + index}
        >
            <Handle value={value} {...restProps} />
        </Tooltip>
    );
};

export default class RangeSliderWidget extends preact.Component {
    constructor(props) {
        super(props);
        this.state = {
            values: props.defaultValues,
        };

        let marks = {};
        marks[this.props.min] = this.props.min;
        marks[this.props.max] = this.props.max;

        this.props.marks = props.marks || marks;
        this.props.name = props.name || {min: 'min', max: 'max'};
        this.handleChange = this.handleChange.bind(this);
    }

    render() {

        return (
            <div id={this.props.id} className={classNames('rc-slider-container', this.props.className)}>
                <Range
                    min={this.props.min}
                    max={this.props.max}
                    defaultValue={this.props.defaultValues}
                    step={this.props.step}
                    onChange={this.handleChange}
                    marks={this.props.marks}
                    count={2}
                    handle={handle}
                />
                <input type="hidden" value={Math.min(...this.state.values)} name={this.props.name.min} />
                <input type="hidden" value={Math.max(...this.state.values)} name={this.props.name.max} />
            </div>
        );
    }

    handleChange(value) {
        this.setState({values: value});
    }
}
