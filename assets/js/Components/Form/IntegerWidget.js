import * as preact from "preact";
import React from "preact/compat";
import renderInput from "./BaseInputWidget";
import {Field} from "redux-form";

const normalizeInteger = (value, previousValue, allValues) => parseInt(value);

const IntegerWidget = (props) => {
    return (<Field
        component={renderInput}
        label={props.label}
        name={props.fieldName}
        required={props.required}
        id={"field-" + props.fieldName}
        placeholder={props.schema.default}
        description={props.schema.description}
        type={props.type}
        normalize={props.normalizer}
        parse={props.parser}
    />);
};

export default IntegerWidget;
