import * as preact from "preact";
import React from 'preact/compat';
import RangeSliderWidget from "./Components/Form/RangeSliderWidget";
import SliderWidget from "./Components/Form/SliderWidget";
import Quill from './Wrappers/Quill';
import "quill/dist/quill.core.css";
import "quill/dist/quill.snow.css";

require('preact/debug'); // TODO: only in dev builds

document.addEventListener('DOMContentLoaded', () => {
    let quill_containers = document.querySelectorAll('.quill');
    quill_containers.forEach((ql) => {

        let config = JSON.parse(ql.dataset.config);
        let textareaId = ql.dataset.id;
        let quill = new Quill(ql, config);

        quill.on('text-change', function(delta, oldDelta, source) {
            document.getElementById(textareaId).value = quill.root.innerHTML;
        });
    });

    let range_sliders = document.querySelectorAll('.react-range-slider');
    range_sliders.forEach((element) => {
        let container = document.createElement('div');
        container.className = 'react-range-slider';
        preact.render(
            <RangeSliderWidget
                min={parseInt(element.dataset.min)}
                max={parseInt(element.dataset.max)}
                id={element.dataset.id}
                name={{min: element.dataset.nameMin, max: element.dataset.nameMax}}
                defaultValues={JSON.parse(element.dataset.values)}
                marks={JSON.parse(element.dataset.marks)}
            />, container);
        element.parentElement.replaceChild(container, element);
    });

    let sliders = document.querySelectorAll('input[type="range"]');
    sliders.forEach((element) => {
        let container = document.createElement('div');
        container.className = 'react-slider';
        preact.render(
            <SliderWidget
                min={parseInt(element.getAttribute('min'))}
                max={parseInt(element.getAttribute('max'))}
                id={element.getAttribute('id')}
                name={element.getAttribute('name')}
                defaultValue={parseInt(element.value)}
                marks={element.dataset.marks ? JSON.parse(element.dataset.marks) : null}
            />, container);
        element.parentElement.replaceChild(container, element);
    });

    document.getElementById('event_zimm_entries-add').addEventListener('click', (e) => {
        e.preventDefault();
        let collection = document.getElementById('event_zimm_entries');
        let prototype = collection.dataset.prototype.replace(/__name__/g, collection.dataset.index);

        let container = document.createElement('template');
        container.innerHTML = prototype;

        let clone = document.importNode(container.content, true);

        collection.appendChild(clone);
        collection.dataset.index = parseInt(collection.dataset.index) + 1;
    });

    document.getElementById('event_zimm_entries').addEventListener('click', (e) => {
        if(e.target && e.target.matches('button.zimm_entry-delete')) {
            e.preventDefault();
            let el = document.getElementById(e.target.dataset.deleteId);
            el.parentNode.removeChild(el);
        }
    });
});

